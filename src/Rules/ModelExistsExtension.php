<?php

namespace Dottystyle\Laravel\Validation\Rules;

class ModelExistsExtension
{
    public function __invoke($attribute, $value, $parameters)
    {
        [$model, $column] = $this->parameters($parameters);

        $rule = new ModelExists($model);

        if ($column) {
            $rule->column($column);
        }

        return $rule->passes($attribute, $value);
    }

    protected function parameters($parameters)
    {
        return $parameters + ['', null];
    }
}