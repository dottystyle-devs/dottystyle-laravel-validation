<?php

namespace Dottystyle\Laravel\Validation\Rules;

use UnexpectedValueException;

class KeyType
{
    public function __invoke($attribute, $value, $parameters)
    {
        if (! isset($parameters[0]) || ! class_exists($parameters[0])) {
            throw new UnexpectedValueException('Missing Eloquent model class to validate key type');
        }

        $keyType = (new $parameters[0])->getKeyType();

        switch ($keyType) {
            case 'string':
                return is_string($value);

            case 'int':
            case 'integer':
                return filter_var($value, FILTER_VALIDATE_INT) !== false;

            default:
                throw new UnexpectedValueException("Unsupported key type: $keyType");
        }
    }
}