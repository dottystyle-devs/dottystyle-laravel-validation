<?php

namespace Dottystyle\Laravel\Validation\Rules;

use Closure;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use InvalidArgumentException;

class ModelExists implements Rule
{
    /**
     * @var mixed
     */
    protected $source;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var \Closure
     */
    protected $resolved;

    /**
     * @var string
     */
    protected $column;

    /**
     * @var bool
     */
    protected $shouldCloneSource = true;

    /**
     * Create a new rule instance.
     *
     * @param mixed $source
     * @param string $message 
     * @param \Closure $modelsResolved (optional)
     * @return void
     * 
     * @throws \InvalidArgumentException
     */
    public function __construct($source, $message = '', Closure $resolved = null)
    {
        $this->source = $this->checkSource($source);
        $this->message = $message ?: self::getDefaultMessage();
        $this->resolved = $resolved;
    }

    /**
     * Get the source object.
     * 
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Check if the given source is supported.
     * 
     * @param mixed $source
     * @throws \InvalidArgumentException
     */
    protected function checkSource($source)
    {
        if (
            $source instanceof Model
            || $source instanceof Builder 
            || $source instanceof Relation 
            || $source instanceof Collection 
            || $source instanceof Closure
        ) {
            return $source;
        }

        if ($this->isModelClass($source)) {
            return $this->checkSource($source::query());
        }

        throw new InvalidArgumentException('Unsupported source type');
    }

    /**
     * Determine whether the given value is valid class path to a model.
     * 
     * @param string $source
     * @return bool
     */
    protected function isModelClass($source)
    {
        return is_string($source)
            && class_exists($source)
            && is_subclass_of($source, Model::class);
    }

    /**
     * Set the column to match the values.
     * 
     * @param string $column
     * @return static
     */
    public function column($column)
    {
        $this->column = $column;

        return $this;
    }

    /**
     * Alias of self::column().
     * 
     * @param string $column
     * @return static
     */
    public function using($column)
    {
        return $this->column($column);
    }

    /**
     * Set whether the source query (applies to eloquent or query builder sources only) should
     * be cloned or not.
     * 
     * @param bool $bool
     * @return self
     */
    public function noClone(bool $bool = true)
    {
        $this->shouldCloneSource = $bool;

        return $this;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->source instanceof Closure) {
            $source = $this->checkSource(
                call_user_func($this->source, $value)
            );
        } else {
            $source = $this->source;
        }

        if ($isArray = is_array($value)) {
            $value = array_unique($value);
        }
        
        if ($source instanceof Model) {
            $result = $this->findByModel($source, $value, $isArray);
        } elseif ($source instanceof Collection) {
            $result = $this->findByCollection($source, $value, $isArray);
        } else {
            $result = $this->findByQuery(
                $this->shouldCloneSource ? $source->clone() : $source, 
                $value,
                $isArray
            );
        }

        if (! $this->foundAll($result, $value, $isArray)) {
            return false;
        }

        if ($this->resolved) {
            call_user_func($this->resolved, $result, $attribute, $value);
        }

        return true;
    }

    /**
     * Find the models using the model.
     * 
     * @param \Illuminate\Database\Eloquent\Model $source
     * @param mixed $value
     * @param bool $isArray
     * @return mixed
     */
    protected function findByModel(Model $source, $value, $isArray)
    {
        return $this->findByQuery(
            $source->newQuery(), $value, $isArray
        );
    }

    /**
     * Find the model by their keys in a collection.
     * 
     * @param \Illuminate\Database\Eloquent\Collection $models
     * @param mixed $value
     * @param bool $isArray
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function findByCollection($models, $value, $isArray)
    {
        if ($this->usesColumn()) {
            $matches = $this->applyWhereColumn($models, $value);

            return $isArray ? $matches : $matches->first();
        }
        
        return $isArray ? $models->only($value) : $models->find($value);
    }

    /**
     * Find by relationship, query builder, or eloquent query builder.
     * 
     * @param mixed $source
     * @param mixed $value
     * @param bool $isArray
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function findByQuery($source, $value, $isArray)
    {
        if ($this->usesColumn()) {
            $this->applyWhereColumn($source, $value);

            return $isArray ? $source->get() : $source->first();
        }

        return $source->find($value);
    }

    /**
     * Apply the where condition given the column and value to match.
     * 
     * @param mixed $source
     * @param mixed $value
     * @return mixed
     */
    protected function applyWhereColumn($source, $value)
    {
        if (is_array($value)) {
            $source->whereIn($this->column, $value);
        } else {
            $source->where($this->column, $value);
        }

        return $source;
    }

    /**
     * Determine whether we found all the models being searched or not.
     * 
     * @param mixed $result
     * @param mixed $value
     * @param bool $isArray
     * @return bool
     */
    protected function foundAll($result, $value)
    {
        if ($result instanceof Collection) {
            return $result->count() >= count($value);
        }

        return $result instanceof Model;
    }

    /**
     * Determine whether to use column for filter or not.
     * 
     * @return bool
     */
    protected function usesColumn()
    {
        return (bool) $this->column;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }

    /**
     * Set the callback to handle the resolved models.
     * 
     * @param \Closure $callback
     * @return static
     */
    public function modelsResolved(Closure $callback)
    {
        $this->resolved = $callback;

        return $this;
    }

    /**
     * Alias of modelsResolved.
     * 
     * @see self::modelsResolved
     */
    public function resolved(Closure $callback)
    {
        return $this->modelsResolved($callback);
    }

    /**
     * Get the macro for the rule.
     * 
     * @return \Closure
     */
    public static function macro()
    {
        $class = self::class;

        return function ($model, $message = '', $callback = null) use ($class) {
            $rule = new $class($model, $message);

            if ($callback) {
                $rule->resolved($callback);
            }

            return $rule;
        };
    }

    public static function getDefaultMessage()
    {
        return 'Invalid :attribute';
    }
}