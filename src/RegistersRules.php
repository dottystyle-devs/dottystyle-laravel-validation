<?php

namespace Dottystyle\Laravel\Validation;

use Dottystyle\Laravel\Validation\Rules\KeyType;
use Dottystyle\Laravel\Validation\Rules\ModelExists;
use Dottystyle\Laravel\Validation\Rules\ModelExistsExtension;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Validation\Rule;

trait RegistersRules
{
    protected function macros()
    {
        Rule::macro('modelExists', ModelExists::macro());
        Rule::macro('model', ModelExists::macro());
    }

    /**
     * Register other validation extensions.
     * 
     * @param \Illuminate\Contracts\Validation\Factory $factory
     * @return void
     */
    protected function extensions(Factory $factory)
    {
        $factory->extend('scalar', function ($attribute, $value) {
            return is_scalar($value);
        }, ':attribute must be a scalar value');

        $factory->extend(
            'key_type', new KeyType, 'Invalid :attribute specified'
        );

        $factory->extend(
            'model', new ModelExistsExtension, ModelExists::getDefaultMessage()
        );
    }
}