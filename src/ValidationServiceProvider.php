<?php

namespace Dottystyle\Laravel\Validation;

use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    use RegistersRules;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->macros();

        $this->app->afterResolving('validator', function ($factory) {
            $this->extensions($factory);
        });
    }

    /**
     * Register services to the container.
     * 
     * @return void
     */
    public function register()
    {
    }
}