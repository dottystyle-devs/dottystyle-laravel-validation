<?php

namespace Dottystyle\Laravel\Validation;

class Attribute
{
    /**
     * Get the attribute name of $name as sibling of the specified attribute.
     * e.g., 
     * 1. Attribute::asSibling('user.id', 'firstname') returns "users.firstname"
     * 2. Attribute::asSibling('user[id]', 'firstname') returns "users[firstname]"
     * 3. Attribute::asSibling('id', 'firstname') returns "firstname"
     * 
     * @param string $attribute
     * @param string $name
     * @return string
     */
    public static function asSibling($attribute, $name)
    {
        if (strpos($attribute, '.') !== false) {
            return preg_replace('/\.[^\.]*$/', ".$name", $attribute);
        }

        if (substr($attribute, -1, 1) === ']') {
            return preg_replace('/(\[[^\]+\])$/', "[$name]", $attribute);
        }

        return $name;
    }
}