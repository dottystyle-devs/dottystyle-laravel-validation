# Changelog

## 1.6.0 (2023-10-27)

### Changed

- Clone eloquent and query builder sources when executing validation.

## Added

- Added `noClone` option to disable cloning of eloquent and query builder when executing  validation.

## 1.5.0 (2023-09-28)

### Added

- `Attribute` helper class

## 1.4.0 (2023-07-28)

### Added

- Support for Laravel 9.x and 10.x
- Alias `using` for `Dottystyle\Laravel\Validation\Rules\ModelExists::column`

## 1.3.0 (2023-04-05)

### Added

- CHANGELOG file
- `model` rule extension e.g., `['user_id' => 'model:\App\Models\User']`
- Support for Laravel 6, 7, and 8
- `Rule::model` as alias for `Rule::modelExists`
- `resolved` as alias for `ModelExists::modelsResolved`
- Support for model class path as source e.g., `Rule::model(User::class)`
- `column` option to use a column to match values instead of primary key e.g., `Rule::model(User::class)->column('email')` or using extension, `['user_id' => 'model:\App\Models\User,email']`

### Changed

- The number of models that matched regardless of source should match the number of distinct values otherwise it will be marked as failed.
